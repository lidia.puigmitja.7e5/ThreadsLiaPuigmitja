﻿using System;
using System.Threading;

namespace ThreadsLiaPuigmitja
{
    class Program
    {
        static void Main()
        {
            string frase1 = "Una vegada hi havia un gat\n";
            string frase2 = "En un lugar de la Mancha\n";
            string frase3 = "Once upon a time in the west\n";

            Thread thread1 = new Thread(() =>
            {
                WriteSentence(frase1);
            });
            Thread thread2 = new Thread(() =>
            {
                thread1.Join();
                WriteSentence(frase2);
            });
            Thread thread3 = new Thread(() =>
            {
                thread2.Join();
                WriteSentence(frase3);
            });

            thread1.Start(); thread2.Start(); thread3.Start();

            Nevera nevera = new Nevera(6);

            Thread thread4 = new Thread(() =>
            {
                Thread.Sleep(1000);
                thread3.Join();
                nevera.OmplirNevera("Anitta");
                
                
            });
            Thread thread5 = new Thread(() =>
            {
                Thread.Sleep(1000);
                thread4.Join();
                nevera.BeureCervesa("Bad Bunny");
               

            });
            Thread thread6 = new Thread(() =>
            {
                Thread.Sleep(1000);
                thread5.Join();
                nevera.BeureCervesa("Lil Nas X");
               
            });
            Thread thread7 = new Thread(() =>
            {
                Thread.Sleep(1000);
                thread6.Join();
                nevera.BeureCervesa("Manuel Turizo");
                
            });

            thread4.Start(); thread5.Start(); thread6.Start(); thread7.Start();
        }

        private static void WriteSentence(string sentence)
        {
            for (int i = 0; i < sentence.Length; i++)
            {
                Console.Write(sentence[i]);
                if (sentence[i] == ' ')
                {
                    Thread.Sleep(1000);
                }
            }
            Thread.Sleep(1000);
        }
        
    }

    class Nevera
    {

        public int cerveses = 0;

        public Nevera(int cerveses) => this.cerveses = cerveses;

        public int OmplirNevera(string nom)
        {
          
            Random r = new Random();
            int omplirCervesa = r.Next(0, 7);
            cerveses += omplirCervesa;
            if (cerveses > 9)
                cerveses = 9;
            Console.WriteLine(nom + " ha omplert " + omplirCervesa + " cerveses");
            Console.WriteLine("Hi ha " + cerveses + " a la nevera.");
            return cerveses;
        }

        public int BeureCervesa(string nom)
        {
            Random r = new Random();
            int beureCervesa = r.Next(0, 7);
            cerveses -= beureCervesa;
            if (cerveses < 0)
                cerveses = 0;
            Console.WriteLine(nom + " ha begut " + beureCervesa + " cerveses");
            Console.WriteLine("Hi ha " + cerveses + " a la nevera.");
            return cerveses;
        }
    }
}
